var q = require("q");
var jwt = require("jsonwebtoken");
var config = require(global.__base + "/config");
var userModel = require(global.__base+ "/models/user");
var auditLogModel = require(global.__base + "/models/audit_log");
var tokenManager = require(global.__base+"/components/token_manager");
var redisClient = require(global.__base+"/components/redis_database");

var authenticate = function(username, password) {
  var deferred = q.defer();

  userModel.findOne({username: username}, function(err, user) {
    if (user == null)
      deferred.reject(new Error("User not found"));
    else if (user.isValidPassword(password))
      deferred.resolve(user);

    deferred.reject(new Error("Invalid password"));
  });

  return deferred.promise;
};

var saveAuditLog = function(user, ip, action) {
  var deferred = q.defer();

  var auditLog = new auditLogModel({
    "ip": ip,
    "action": action,
    "username": user.username
  });

  auditLog.save(function(err) {
    if (err)
      deferred.reject(err);
    else
      deferred.resolve(user);
  });

  return deferred.promise;
};

exports.logout = function(req, res) {
  if (req.user) {
    tokenManager.expireToken(req.headers);
    delete req.user;

    res.status(200).json({success:1});
  }
  else {
    res.send(401).json({error:1});
  }
};


var getToken = function(user) {
  var deferred = q.defer();

  var userInfo = {
    "username": user.username,
    "isAdmin": user.isAdmin
  };
  var token = jwt.sign(userInfo, config.secretToken, { expiresInMinutes: tokenManager.TOKEN_EXPIRATION });

  if (token){
    deferred.resolve(token);
  }
  else
    deferred.reject(new Error("Bad token"));

  return deferred.promise;
};

exports.verifyToken = function(req, res, next){
  return tokenManager.verifyToken(req, res, next);
};

exports.login = function(req, res) {
  if (!req.body)
    return res.sendStatus(400);

  var username = req.body.username;
  var password = req.body.password;

  authenticate(username, password).then(function(user) {
    return saveAuditLog(user, req.ip, "login_success");
  }).then(function(user) {
    return getToken(user);
  }).then(function(token) {
    res.json({"token": token});
  }).catch(function(err){
  console.log(err);
    res.status(403).json({"error": 1});

  });
};
