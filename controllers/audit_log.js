var q = require("q");
var auditLogModel = require(global.__base + "/models/audit_log");

var list = function() {
  var deferred = q.defer();

  auditLogModel.find({}, function(err, events) {
    if (err)
      deferred.reject(err);
    else
      deferred.resolve(events || []);
  });

  return deferred.promise;
};

exports.get = function(req, res) {

  if (req.user) {
    if (req.user.isAdmin) {
      list().then(function(events) {
        res.json({events: events});
      }).catch(function(err) {
        res.status(403).json({error: 1});
      });
    } else {
      res.status(403).json({error: 1});
    }
  }
  else {
    res.status(401).json({error: 1});
  }
};
