var redisClient = require('./redis_database').redisClient;
var TOKEN_EXPIRATION = 60;
var TOKEN_EXPIRATION_SEC = TOKEN_EXPIRATION * 60;

// Middleware for token verification
var getToken = function(headers) {
  if (headers && headers.authorization) {
    var authorization = headers.authorization;
    var part = authorization.split(' ');

    if (part.length == 2) {
      var token = part[1];
      return token;
    }
    else {
      return null;
    }
  }
  else {
    return null;
  }
};

exports.verifyToken = function(req, res, next) {
  var token = getToken(req.headers);

  console.log(token);
  redisClient.get(token, function(err, reply) {
    if (err)
      res.status(500).json({error:1});
    if (reply != null) {
      console.log("redis object: ", reply);
      res.status(401).json({error:1});
    } else {
      res.status(200).json({success:1});
    }

    next();
  });
};

exports.expireToken = function(headers) {
  var token = getToken(headers);
  if (token != null) {
    redisClient.set(token, { is_expired: true });
    redisClient.expire(token, TOKEN_EXPIRATION_SEC);
  }
};

exports.TOKEN_EXPIRATION = TOKEN_EXPIRATION;
exports.TOKEN_EXPIRATION_SEC = TOKEN_EXPIRATION_SEC;
