var mongoose = require('mongoose'),
    crypto = require('crypto'),
    uuid = require('node-uuid'),
    Schema = mongoose.Schema;

var userSchema = new Schema({
    username: { type: String, required: true, unique: true },
    salt: { type: String, required: true, default: uuid.v1 },
    passwordHash: { type: String, required: true },
    isAdmin:{ type:Boolean, default: false }
});

var hash = function(passwd, salt) {
    return crypto.createHmac('sha512', salt).update(passwd).digest('hex');
};

userSchema.methods.setPassword = function(passwordString) {
    this.passwordHash = hash(passwordString, this.salt);
};

userSchema.methods.isValidPassword = function(passwordString) {
    return this.passwordHash === hash(passwordString, this.salt);
};

mongoose.model('user', userSchema);
module.exports = mongoose.model('user');
