var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var auditLog = new Schema({
    ip: { type: String, required: true },
    datetime: { type: String, required: true, default: new Date() },
    action: { type: String, required: true },
    username: { type: String, required: true }
});

module.exports = mongoose.model('auditLog', auditLog);