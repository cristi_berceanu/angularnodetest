global.__base = __dirname;
var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var config = require("./config");
var jwt = require('express-jwt');

mongoose.connect(config.db.mongodb);

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(express.static(path.join(__dirname, 'public')));


// enabling CORS requests
app.all('*', function(req, res, next) {
  res.set('Access-Control-Allow-Origin', '*');
  res.set('Access-Control-Allow-Credentials', true);
  res.set('Access-Control-Allow-Methods', 'GET, POST, DELETE, PUT');
  res.set('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Authorization');
  if ('OPTIONS' == req.method) return res.sendStatus(200);
  next();
});

var controllers = {};
controllers.users = require('./controllers/login');
controllers.auditLog = require('./controllers/audit_log');
var tokenManager = require('./components/token_manager');
app.post('/login', controllers.users.login);
app.post('/logout', jwt({secret: config.secretToken}), controllers.users.logout);
app.get('/audit_log', jwt({secret: config.secretToken}), controllers.auditLog.get);
app.get('/verify_token', controllers.users.verifyToken);
app.use(express.static(path.join(__dirname, 'app')));

// development
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {} // no stacktraces leaked to user
  });
});


module.exports = app;
