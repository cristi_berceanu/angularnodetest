var should = require('should');
var assert = require('assert');
var request = require('supertest');
var mongoose = require('mongoose');
var redisClient = require('../components/redis_database').redisClient;
describe('Routing', function() {
  var url = 'http://localhost:3000';
  var adminUser = {
    "username": "administrator",
    "password": "password"
  };

  var testUser = {
    "username": "tester",
    "password": "password"
  };

  var loginUser = function(user, callback) {
    request(url).post('/login').type('json').send(JSON.stringify(user)).end(function(err, res) {
      var token = res.body["token"];
      callback(token);
    });
  };

  var logoutUser = function(token, callback){
    request(url).post('/logout').type('json').send({}).set('Authorization', 'Bearer ' + token).end(function(err, res){
      callback();
    });
  };

  before(function(done) {
    done();
  });

  describe('Test: Account', function() {
    it('shouldn\'t login user with wrong password', function(done) {
      var wrongUser = {
        "username": "administrator",
        "password": "bad password"
      };

      request(url).post('/login').type('json').send(JSON.stringify(wrongUser)).end(function(err, res) {
        if (err)
          throw err;

        assert.equal(res.body.error, 1);
        done();
      })
        .expect('Content-Type', /json/)
        .expect(403);
    });

    it('should login user with password', function(done) {
      request(url).post('/login').type('json').send(JSON.stringify(adminUser)).end(function(err, res) {
        if (err)
          throw err;

        res.body.should.hasOwnProperty("token");
        done();
      })
        .expect('Content-Type', /json/)
        .expect(200);
    });


    it('should validate a unexpired token', function(done) {
      loginUser(adminUser, function (token) {
        request(url).get('/verify_token').type('json').send({}).set('Authorization', 'Bearer ' + token)
          .end(function (err, res) {
            if (err)
              throw err;
            done();
          })
          .expect('Content-Type', /json/)
          .expect(200);

      })
    });

    it('should logout user', function(done) {
      loginUser(adminUser, function(token) {
        request(url).post('/logout').type('json').send({}).set('Authorization', 'Bearer ' + token)
          .end(function(err, res) {
            if (err)
              throw err;
            done();
          })

          .expect('Content-Type', /json/)
          .expect(200);
      });
    });

    it('should not validate an expired token', function(done) {
      loginUser(adminUser, function (token) {
        request(url).get('/verify_token').type('json').send({}).set('Authorization', 'Bearer ' + token)
          .end(function (err, res) {
            if (err)
              throw err;
            done();
          })
          .expect('Content-Type', /json/)
          .expect(401);

      })
    });



  });

  describe('Test: Audit log', function() {
    it('should return 401 for anonymous user', function(done) {
      request(url).get('/audit_log').type('json').send({})
        .end(function(err, res) {
          done();
        })
        .expect(401);
    });



    it('should show 403 for user with no admin permissions', function(done) {
      loginUser(testUser, function(token) {
        request(url).get('/audit_log').type('json').send({}).set('Authorization', 'Bearer ' + token)
          .end(function(err, res) {
            if (err)
              throw err;

            done();
          })
          .expect('Content-Type', /json/)
          .expect(403);
      });
    });

    it('should return audit log for admin user', function(done) {
      loginUser(adminUser, function(token) {
        request(url).get('/audit_log').type('json').send({}).set('Authorization', 'Bearer ' + token)
          .end(function(err, res) {
            if (err){
              throw err;
            }
            res.body.should.hasOwnProperty("events");
            done();
          })
          .expect('Content-Type', /json/)
          .expect(200);
      });
    });
  });

});
