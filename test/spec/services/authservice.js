'use strict';

describe('Service: AuthService', function () {

  // load the service's module
  beforeEach(module('angularNodeTestApp'));

  // instantiate service
  var AuthService,
    $httpBackend,
    localStorageService;
  beforeEach(inject(function (_AuthService_, $injector, _localStorageService_) {
    $httpBackend = $injector.get('$httpBackend');
    AuthService = _AuthService_;
    localStorageService = _localStorageService_;
  }));

  it('should test successful login', function () {
    $httpBackend.expectPOST('http://localhost:3000/login').respond({
      token:'token'
    });
    AuthService.login('administrator', 'password').then(function(data){
      expect(data.token).toEqual('token');
    });
    $httpBackend.flush();
    expect(localStorageService.get('user').hasOwnProperty('token')).toBeTruthy();
  });

  it('should test unsuccessful login', function(){
    $httpBackend.expectPOST('http://localhost:3000/login').respond(403);
    AuthService.login().catch(function(data){
      expect(data.error).toEqual(1);
    });
    $httpBackend.flush();

  });
  afterEach(function(){
    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
  });

});
