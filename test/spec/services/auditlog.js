'use strict';

describe('Service: AuditLog', function () {

  // load the service's module
  beforeEach(module('angularNodeTestApp'));

  // instantiate service
  var AuditLog;
  beforeEach(inject(function (_AuditLog_) {
    AuditLog = _AuditLog_;
  }));

  it('should do something', function () {
    expect(!!AuditLog).toBe(true);
  });

});
