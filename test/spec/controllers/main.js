'use strict';

describe('Controller: MainCtrl', function () {

  // load the controller's module
  beforeEach(module('angularNodeTestApp'));

  var MainCtrl,
    scope,
    $httpBackend;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope, $injector) {
    $httpBackend = $injector.get('$httpBackend');
    scope = $rootScope.$new();
    MainCtrl = $controller('MainCtrl', {
      $scope: scope
    });
  }));

  it('should check if user property is set', function () {

    expect(Object.keys(scope.user).length).toBeGreaterThan(0);
  });

  it('should load the audit log list', function () {

    $httpBackend.expectGET('http://localhost:3000/audit_log').respond({
      events: [1,2,3]
    });
    $httpBackend.flush();
    expect(scope.auditLog.length).toBeGreaterThan(0);

  });
  afterEach(function(){
    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
  });
});
