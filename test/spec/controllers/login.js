'use strict';

describe('Controller: LoginCtrl', function () {

  // load the controller's module
  beforeEach(module('angularNodeTestApp'));

  var LoginCtrl,
    scope,
    $httpBackend;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope, $injector) {
    $httpBackend = $injector.get('$httpBackend');
    scope = $rootScope.$new();
    LoginCtrl = $controller('LoginCtrl', {
      $scope: scope
    });
  }));

  it('should check if LoginCtrl has login() function', function () {
    expect(scope.hasOwnProperty('login')).toBeTruthy();
  });

  it('should show error message in case of bad login', function () {
    $httpBackend.expectPOST('http://localhost:3000/login').respond(403);
    scope.username = 'administrator';
    scope.password = 'bad password';
    scope.login();
    $httpBackend.flush();
    expect(!!scope.errorMessage).toBeTruthy();
  });
  afterEach(function(){
    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
  })
});
