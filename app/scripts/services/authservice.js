'use strict';

/**
 * @ngdoc service
 * @name angularNodeTestApp.AuthService
 * @description
 * # AuthService
 * Factory in the angularNodeTestApp.
 */
angular.module('angularNodeTestApp')
  .factory('AuthService', function ($q, $http, localStorageService) {

    var deferred = $q.defer();
    var login = function (username, password) {
      $http.post('http://localhost:3000/login', {
        'username':username, password:password
      }).
        success(function(data, status, headers, config){
          var user = {
            username: username,
            token: data.token
          };
          localStorageService.set('user', user);
          deferred.resolve(user);
        }).
        error(function(data, status, headers, config){
          deferred.reject({error:1});
        });
      return deferred.promise;
    };
    var logout = function(){
      localStorageService.clear();
    };

    var verifyToken = function () {
      var deferred = $q.defer();
      $http.get('http://localhost:3000/verify_token', {
        headers: {
          'Authorization': 'Bearer ' + localStorageService.get('user').token
        }
      }).
        success(function(data, status, headers, config){
          deferred.resolve(data);
        }).
        error(function(data, status, headers, config){
          deferred.reject({error:1});
        });
      return deferred.promise;
    };
    return {
      login: login,
      logout: logout,
      verifyToken: verifyToken
    };
  });
