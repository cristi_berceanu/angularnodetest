'use strict';

/**
 * @ngdoc service
 * @name angularNodeTestApp.AuditLog
 * @description
 * # AuditLog
 * Factory in the angularNodeTestApp.
 */
angular.module('angularNodeTestApp')
  .factory('AuditLog', function ($resource) {

    return $resource('http://localhost:3000/audit_log', {}, {
      "get": {method:"GET", isArray: false, cache: false, invisible: false, is_api_call:true},

    });

  });
