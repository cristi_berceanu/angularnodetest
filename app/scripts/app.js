'use strict';

/**
 * @ngdoc overview
 * @name angularNodeTestApp
 * @description
 * # angularNodeTestApp
 *
 * Main module of the application.
 */
angular
  .module('angularNodeTestApp', [
    'ngMessages',
    'ngResource',
    'ngRoute',
    'LocalStorageModule'
  ])
  .config(function ($routeProvider, localStorageServiceProvider, $httpProvider) {
    localStorageServiceProvider
      .setPrefix('angularNodeTest')
      .setStorageType('localStorage')
      .setNotify(true, true);

    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        resolve: {
          verifyToken: ['AuthService', function(AuthService){
            return AuthService.verifyToken();
          }]
        }
      })
      .when('/login', {
        templateUrl: 'views/login.html',
        controller: 'LoginCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
    $httpProvider.interceptors.push(function($q){
      return {
        'responseError': function(response){
          if(response.status == 403){
            console.log("Forbidden");
          }

          if(response.status == 401){
            console.log("Unauthorized");
          }
          return $q.reject(response);
        },
        'requestError': function(rejection){
          console.log(rejection);
          return $q.reject(rejection);
        }
      }
    });
  });
