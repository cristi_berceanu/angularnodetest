'use strict';

/**
 * @ngdoc function
 * @name angularNodeTestApp.controller:LoginCtrl
 * @description
 * # LoginCtrl
 * Controller of the angularNodeTestApp
 */
angular.module('angularNodeTestApp')
  .controller('LoginCtrl', function ($scope, AuthService, $location) {

    $scope.login = function(){
      AuthService.login($scope.username, $scope.password).
        then(function(){
          $location.path('/');
        }).
        catch(function(err){
          $scope.errorMessage = 'Username or password invalid';
          console.log(err)
        })
    }

  });
