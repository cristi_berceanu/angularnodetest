'use strict';

/**
 * @ngdoc function
 * @name angularNodeTestApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the angularNodeTestApp
 */
angular.module('angularNodeTestApp')
  .controller('AboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
