'use strict';

/**
 * @ngdoc function
 * @name angularNodeTestApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the angularNodeTestApp
 */
angular.module('angularNodeTestApp')
  .controller('MainCtrl', function ($scope, localStorageService, $location, AuditLog) {
    AuditLog.get().$promise.then(function(data){

    }).catch(function(err){

    });
    $scope.user = localStorageService.get('user');

    $scope.logout = function () {
      localStorageService.clearAll();
      $location.path('/login');
    }



  });
